package ittol.ciudadesinteligentes;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;

import com.ibm.watson.developer_cloud.discovery.v1.Discovery;
import com.ibm.watson.developer_cloud.discovery.v1.model.*;
import com.ibm.watson.developer_cloud.http.HttpMediaType;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class Entry extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        CitizenFragment.OnFragmentInteractionListener,
        PredixFragment.OnFragmentInteractionListener,
        GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = "LOCATION_SERVICE_SaNC";

    private static GoogleApiClient mGoogleApiClient;
    private static Location location;

    private static SharedPreferences settings;
    private static SharedPreferences.Editor editor;

    private FrameLayout frameEntry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "N/A", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        frameEntry = (FrameLayout) findViewById(R.id.frameEntry);

        startService(new Intent(this, LocationService.class));

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        System.out.println("Connected...");
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        System.out.println("Done...");
                    }
                })
                .build();
        mGoogleApiClient.connect();

        settings = getSharedPreferences("UserInfo", 0);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.entry, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.actionSettings:
                break;
            case R.id.actionAbout:
                break;
            case R.id.actionExit:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        // Fragment update.
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        switch (item.getItemId()) {
            case R.id.navCitizen:
                fragmentTransaction.replace(R.id.frameEntry, new CitizenFragment());
                break;
            case R.id.navPredix:
                fragmentTransaction.replace(R.id.frameEntry, new PredixFragment());
                break;
            case R.id.navShare:
                break;
        }
        fragmentTransaction.commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) { }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) { }

    public static void placeFromGAPI (Location source) {
        location = source;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        System.out.println("Getting location data...");
        PendingResult<PlaceLikelihoodBuffer> result = null;
        try {
            result = Places.PlaceDetectionApi
                    .getCurrentPlace(mGoogleApiClient, null);
        } catch (SecurityException se) {
            System.out.println(se);
        }
        result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
            @Override
            public void onResult(PlaceLikelihoodBuffer likelyPlaces) {
                System.out.println("Analyzing location data...");
                PlaceLikelihood likely = null;
                double likehood = 0;
                for (PlaceLikelihood placeLikelihood : likelyPlaces) {
                    Log.i(TAG, String.format("Place '%s' has likelihood: %g",
                            placeLikelihood.getPlace().getName(),
                            placeLikelihood.getLikelihood()));
                    if (placeLikelihood.getLikelihood() > likehood) {
                        likely = placeLikelihood;
                        likehood = likely.getLikelihood();
                    }
                }
                new postOnDiscovery(location, likelyPlaces, likely).execute();
            }
        });
    }


    public static class postOnDiscovery extends AsyncTask<String, Void, String> {

        Location location;
        PlaceLikelihood likely;
        PlaceLikelihoodBuffer likelyPlaces;

        public postOnDiscovery(Location source, PlaceLikelihoodBuffer sourcePlaces, PlaceLikelihood sourcePlace) {
            location = source;
            likely = sourcePlace;
            likelyPlaces = sourcePlaces;
        }

        @Override
        protected String doInBackground(String... params) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            Discovery discovery = new Discovery("2017-11-07");
            discovery.setEndPoint("https://gateway.watsonplatform.net/discovery/api");
            discovery.setUsernameAndPassword("432e4fc5-6a9a-467a-b75c-efc0a996fb89", "S4MXZnc4hpU0");
            String environmentId = null;
            String configurationId = null;
            String collectionId = null;
            String documentId = null;

            //See if an environment already exists
            System.out.println("Check if environment exists");
            ListEnvironmentsOptions listOptions = new ListEnvironmentsOptions.Builder().build();
            ListEnvironmentsResponse listResponse = discovery.listEnvironments(listOptions).execute();
            for (Environment environment : listResponse.getEnvironments()) {
                //look for an existing environment that isn't read only
                if (!environment.isReadOnly()) {
                    environmentId = environment.getEnvironmentId();
                    System.out.println("Found existing environment ID: " + environmentId);
                    break;
                }
            }

            if (environmentId == null) {
                System.out.println("No environment found, creating new one...");
                //no environment found, create a new one (assuming we are a FREE plan)
                String environmentName = "watson_developer_cloud_test_environment";
                CreateEnvironmentOptions createOptions = new CreateEnvironmentOptions.Builder()
                        .name(environmentName)
                        .size(0L)  // FREE
                        .build();
                Environment createResponse = discovery.createEnvironment(createOptions).execute();
                environmentId = createResponse.getEnvironmentId();
                System.out.println("Created new environment ID: " + environmentId);

                //wait for environment to be ready
                System.out.println("Waiting for environment to be ready...");
                boolean environmentReady = false;
                while (!environmentReady) {
                    GetEnvironmentOptions getEnvironmentOptions = new GetEnvironmentOptions.Builder(environmentId).build();
                    Environment getEnvironmentResponse = discovery.getEnvironment(getEnvironmentOptions).execute();
                    environmentReady = getEnvironmentResponse.getStatus().equals(Environment.Status.ACTIVE);
                    try {
                        if (!environmentReady) {
                            Thread.sleep(500);
                        }
                    } catch (InterruptedException e) {
                        throw new RuntimeException("Interrupted", e);
                    }
                }
                System.out.println("Environment Ready!");
            }

            //find the default configuration
            System.out.println("Finding the default configuration");
            ListConfigurationsOptions listConfigsOptions = new ListConfigurationsOptions.Builder(environmentId).build();
            ListConfigurationsResponse listConfigsResponse = discovery.listConfigurations(listConfigsOptions).execute();
            for (Configuration configuration : listConfigsResponse.getConfigurations()) {
                if (configuration.getName().equals("Default Configuration")) {
                    configurationId = configuration.getConfigurationId();
                    System.out.println("Found default configuration ID: " + configurationId);
                    break;
                }
            }

            Collection collection;
            String collectionName = "my_watson_developer_cloud_collection";
            editor = settings.edit();
            if (settings.getString("collectionID", "0").equals("0")){
                //create a new collection
                System.out.println("Creating a new collection...");
                CreateCollectionOptions createCollectionOptions =
                        new CreateCollectionOptions.Builder(environmentId, collectionName)
                                .configurationId(configurationId)
                                .build();

                collection = discovery.createCollection(createCollectionOptions).execute();
                collectionId = collection.getCollectionId();
                System.out.println("Created a collection ID: " + collectionId);

                editor.putString("collectionID", collectionId);
                editor.commit();
            }
            else {
                //load existing collection
                System.out.println("Loading existing collection...");
                GetCollectionOptions getCollectionOptions =
                        new GetCollectionOptions.Builder(environmentId, settings.getString("collectionID", "0"))
                                .build();
                collection = discovery.getCollection(getCollectionOptions).execute();
                collectionId = collection.getCollectionId();
                System.out.println("Loaded collection with ID: " + collectionId);
            }

            //wait for the collection to be "available"
            System.out.println("Waiting for collection to be ready...");
            boolean collectionReady = false;
            while (!collectionReady) {
                GetCollectionOptions getCollectionOptions =
                        new GetCollectionOptions.Builder(environmentId, collectionId).build();
                Collection getCollectionResponse = discovery.getCollection(getCollectionOptions).execute();
                collectionReady = getCollectionResponse.getStatus().equals(Collection.Status.ACTIVE);
                try {
                    if (!collectionReady) {
                        Thread.sleep(500);
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException("Interrupted", e);
                }
            }
            System.out.println("Collection Ready!");

            System.out.println("Creating a new document...");
            char cc = '"';
            String documentJson = "{" + System.lineSeparator()
                    + cc + "provider" + cc + ":" + cc + location.getProvider() + cc + ',' + System.lineSeparator()
                    + cc + "latitude" + cc + ":" + cc + location.getLatitude() + cc + ',' + System.lineSeparator()
                    + cc + "longitude" + cc + ":" + cc + location.getLongitude() + cc + ',' + System.lineSeparator()
                    + cc + "placeName" + cc + ":" + cc + likely.getPlace().getName() + cc + ',' + System.lineSeparator()
                    + cc + "mainType" + cc + ":" + cc + likely.getPlace().getPlaceTypes().get(0) + cc + ',' + System.lineSeparator()
                    + cc + "address" + cc + ":" + cc + likely.getPlace().getAddress() + cc + ',' + System.lineSeparator()
                    + cc + "price" + cc + ":" + cc + likely.getPlace().getPriceLevel() + cc + ',' + System.lineSeparator()
                    + cc + "rating" + cc + ":" + cc + likely.getPlace().getRating() + cc + ',' + System.lineSeparator()
                    + cc + "likelihood" + cc + ":" + cc + likely.getLikelihood() + cc
                    + System.lineSeparator() + "}";
            System.out.println("JSON Document: " + System.lineSeparator() + documentJson);
            InputStream documentStream = new ByteArrayInputStream(documentJson.getBytes());

            AddDocumentOptions.Builder createDocumentBuilder =
                    new AddDocumentOptions.Builder(environmentId, collectionId);
            createDocumentBuilder.filename(String.valueOf(System.currentTimeMillis() / 1000));
            createDocumentBuilder.file(documentStream).fileContentType(HttpMediaType.APPLICATION_JSON);
            DocumentAccepted createDocumentResponse = discovery.addDocument(createDocumentBuilder.build()).execute();
            documentId = createDocumentResponse.getDocumentId();
            System.out.println("Created a document ID: " + documentId);

            //wait for document to be ready
            System.out.println("Waiting for document to be ready...");
            boolean documentReady = false;
            while (!documentReady) {
                GetDocumentStatusOptions getDocumentStatusOptions =
                        new GetDocumentStatusOptions.Builder(environmentId, collectionId, documentId).build();
                DocumentStatus getDocumentResponse = discovery.getDocumentStatus(getDocumentStatusOptions).execute();
                documentReady = !getDocumentResponse.getStatus().equals(DocumentStatus.Status.PROCESSING);
                try {
                    if (!documentReady) {
                        Thread.sleep(500);
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException("Interrupted");
                }
            }
            System.out.println("Document Ready!");

            //query document
            System.out.println("Querying the collection...");
            QueryOptions.Builder queryBuilder = new QueryOptions.Builder(environmentId, collectionId);
            queryBuilder.query("field:value");
            QueryResponse queryResponse = discovery.query(queryBuilder.build()).execute();

            //print out the results
            System.out.println("Query Results:");
            System.out.println(queryResponse);
            /*
            //cleanup the collection created
            System.out.println("Deleting the collection...");
            DeleteCollectionOptions deleteOptions =
                    new DeleteCollectionOptions.Builder(environmentId, collectionId).build();
            discovery.deleteCollection(deleteOptions).execute();
            System.out.println("Collection deleted!");
            */
            likelyPlaces.release();

            return "Executed...";
        }

        @Override
        protected void onPostExecute(String result) {
            System.out.println("Finished execution...");
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }
}
